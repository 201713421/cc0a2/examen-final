package com.example.mytrivia;

import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bosphere.filelogger.FL;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = MainActivity.class.getSimpleName();

    TextView textViewQuestion;
    ImageView imageViewGame;
    Button buttonTrue, buttonFalse, buttonBefore, buttonNext;
    ArrayList<Integer> image = new ArrayList<>();
    int i = 0;
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FL.i(TAG, "Application created");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewQuestion = findViewById(R.id.question);
        imageViewGame = findViewById(R.id.game_image);
        buttonTrue = findViewById(R.id.button_true);
        buttonFalse = findViewById(R.id.button_false);
        buttonBefore = findViewById(R.id.button_before);
        buttonNext = findViewById(R.id.button_next);
        relativeLayout = findViewById(R.id.relative_layout);

        fillArray();

        Resources res = getResources();

        textViewQuestion.setText(res.getStringArray(R.array.questions)[0]);
        imageViewGame.setImageResource(image.get(0));

        buttonNext.setOnClickListener(v -> {
            if (i != 5){
                i++;
            }
            else {
                i = 0;
            }
            textViewQuestion.setText(res.getStringArray(R.array.questions)[i]);
            imageViewGame.setImageResource(image.get(i));
            FL.i("Clicking Button Next and changing image and text");
        });

        buttonBefore.setOnClickListener(v -> {
            if (i != 0){
                i--;
                textViewQuestion.setText(res.getStringArray(R.array.questions)[i]);
                imageViewGame.setImageResource(image.get(i));
                FL.i("Clicking Button Before and changing image and text");
            }
            else {
                FL.i("Clicking Button Before but not doing nothing");
            }
        });

        buttonTrue.setOnClickListener(v -> {
            if(i == 0 || i == 3 || i == 5) {
                Snackbar.make(relativeLayout, R.string.correct_estatement, Snackbar.LENGTH_INDEFINITE).setAction(R.string.close, v1 -> {
                }).show();
                FL.i("Clicking Button True Correct");
            }
            else {
                Snackbar.make(relativeLayout,R.string.incorrect_estatement,Snackbar.LENGTH_INDEFINITE).setAction(R.string.close, v1 -> {}).show();
                FL.i("Clicking Button True Incorrect");
            }
        });

        buttonFalse.setOnClickListener(v -> {
            if(i == 0 || i == 3 || i == 5) {
                Snackbar.make(relativeLayout, R.string.incorrect_estatement, Snackbar.LENGTH_INDEFINITE).setAction(R.string.close, v1 -> {
                }).show();
                FL.i("Clicking Button True Incorrect");
            }
            else {
                Snackbar.make(relativeLayout,R.string.correct_estatement,Snackbar.LENGTH_INDEFINITE).setAction(R.string.close, v1 -> {}).show();
                FL.i("Clicking Button True Correct");
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        FL.i(TAG, "Application paused");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FL.i(TAG, "Application stopped");
    }

    protected void fillArray(){
        image.add(R.drawable.halflife);
        image.add(R.drawable.resident);
        image.add(R.drawable.wii);
        image.add(R.drawable.halo);
        image.add(R.drawable.starcraft_2);
        image.add(R.drawable.kratos);
    }
}